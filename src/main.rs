use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::{self, BufReader, Write as IOWrite};
use std::option::Option;
use std::string::String;
use std::sync::{mpsc, Arc, Barrier};
use std::thread;
use std::time::Duration;
use std::vec::Vec;

use chrono::prelude::NaiveDate;
use regex::Regex;
use threadpool::ThreadPool;

#[derive(Clone)]
struct Video {
    url: String,
    title: String,
    duration: Duration,
    channel: String,
    published: NaiveDate,
}

#[derive(Debug)]
enum ParseError {
    Title,
    Duration,
    Channel,
    Date,
}

fn parse(url: &str, html: &str) -> Result<Video, ParseError> {
    println!("{}", url);

    // Parse title.
    let re = Regex::new(r"<title>(.+)</title>").unwrap();
    let title = re
        .captures(html)
        .ok_or(ParseError::Title)?
        .get(1)
        .ok_or(ParseError::Title)?
        .as_str()
        .to_owned();

    // Parse duration.
    let re = Regex::new(r#"lengthSeconds\\":\\"(\d+)\\""#).unwrap();
    let duration: Duration = Duration::from_secs(
        re.captures(html)
            .ok_or(ParseError::Duration)?
            .get(1)
            .ok_or(ParseError::Duration)?
            .as_str()
            .parse::<u64>()
            .map_err(|_| ParseError::Duration)?,
    );

    // Parse channel name.
    let re = Regex::new(r#"author\\":\\"(.+?)\\""#).unwrap();
    let channel = re
        .captures(html)
        .ok_or(ParseError::Channel)?
        .get(1)
        .ok_or(ParseError::Channel)?
        .as_str()
        .to_owned();

    // Parse published date.
    let re = Regex::new(r#"datePublished" content="(.+?)""#).unwrap();
    let published = NaiveDate::parse_from_str(
        re.captures(html)
            .ok_or(ParseError::Date)?
            .get(1)
            .ok_or(ParseError::Date)?
            .as_str(),
        "%Y-%m-%d",
    )
    .map_err(|_| ParseError::Date)?;

    // Constructs Video instance and returns it back.
    Ok(Video {
        url: url.to_owned(),
        title,
        duration,
        channel,
        published,
    })
}

fn load_playlist(path: &str) -> Vec<String> {
    // Filters lines by if it starts with "#".
    fn parse_urls(line: io::Result<String>) -> Option<String> {
        let line = line.ok().unwrap();

        if !line.starts_with('#') {
            return Some(line.trim().to_string());
        }

        None
    }

    // Open and load the file.
    let f = File::open(path);
    let reader = BufReader::new(f.unwrap());

    // Read lines and filter the URLs.
    reader
        .lines()
        .filter_map(parse_urls)
        .collect::<Vec<String>>()
}

fn save_playlist(path: &str, videos: &[Video]) -> Result<(), std::io::Error> {
    fn format_title(video: &Video) -> String {
        format!(
            "({} | {}) - {}",
            video.channel, video.published, video.title,
        )
    }

    let new_path = format!("new_{}", path);

    let mut f = File::create(new_path).unwrap();

    // Write header.
    f.write_all(b"# Created by YTitler\n")?;
    f.write_all(b"# See: https://gitlab.com/n1_/ytitler\n\n")?;
    f.write_all(b"#EXTM3U\n")?;

    // Write content.
    for v in videos {
        f.write_fmt(format_args!(
            "#EXTINF:{},{}\n",
            v.duration.as_secs(),
            format_title(v)
        ))?;
        f.write_fmt(format_args!("{}\n", v.url))?;
    }

    f.sync_all()?;

    Ok(())
}

fn remove_duplicities(urls: &mut Vec<String>) {
    // Walks thru vector and with help of
    // hash set determines which items are
    // already in the vector and which are
    // not. Preserves order of the vector
    // items.
    let mut set = HashSet::new();
    urls.retain(|i| set.insert(i.clone()));
}

fn to_chunks(mut urls: Vec<String>) -> Vec<Vec<String>> {
    let mut chunks: Vec<Vec<String>> = vec![];
    const LIMIT: f32 = 10.0;
    let steps = ((urls.len() as f32 / LIMIT) as f32).ceil() as i32;
    let mut i = 0;

    // Walk thru vector and grab LIMIT or the rest of
    // items and put them into chunks under a separated
    // vector.
    while i < steps {
        let to_drain = if urls.len() >= LIMIT as usize {
            LIMIT as usize
        } else {
            urls.len()
        };

        chunks.push(urls.drain(0..to_drain).collect());

        i += 1;
        println!("A chunk: {}/{}", i, steps);
    }

    chunks
}

fn fetch(chunks: Vec<Vec<String>>) -> Vec<Video> {
    fn fetch_one(url: &str, sender: &mpsc::Sender<Video>) {
        fn is_available(html: &str) -> bool {
            html.contains("'IS_UNAVAILABLE_PAGE': false")
        }

        // Fetch URL and check the response before next step.
        let mut response = reqwest::get(url).unwrap();
        println!("Response: {}", response.status());

        if response.status().is_success() {
            let mut html = String::new();
            response.read_to_string(&mut html).unwrap();
            println!("Body: {}", html.len());

            if is_available(&html) {
                // Send video to channel.
                let video = parse(url, &html).unwrap();
                sender.send(video).unwrap();
            }
        }
    }

    fn fetch_chunk(chunk: Vec<String>) -> Vec<Video> {
        let (sender, receiver) = mpsc::channel();

        println!("--- New pool ---");
        let barrier = Arc::new(Barrier::new(chunk.len()));
        let pool = ThreadPool::new(chunk.len());

        for url in chunk {
            let barrier = barrier.clone();
            let s = sender.clone();

            // ThreadPool + request.
            pool.execute(move || {
                fetch_one(&url, &s);
                barrier.wait();
            })
        }

        // Wait for all the threads.
        barrier.wait();

        // Wait before next run.
        thread::sleep(Duration::from_millis(500));

        // Return collected videos.
        let mut videos = vec![];

        for v in receiver.try_iter() {
            videos.push(v);
        }

        videos
    }

    let mut videos = vec![];

    for ch in chunks {
        let mut chunk_videos = fetch_chunk(ch);
        videos.append(&mut chunk_videos);
    }

    println!("Collected videos: {}", videos.len());

    videos
}

fn main() {
    let playlist = String::from("youtube_streams.m3u");
    let mut urls: Vec<String> = load_playlist(&playlist);
    println!("{}", urls.len());

    remove_duplicities(&mut urls);
    println!("{}", urls.len());

    let chunks = to_chunks(urls);
    println!("{}", chunks.len());

    let videos = fetch(chunks);
    println!("{}", videos.len());

    save_playlist(&playlist, &videos).unwrap();

    // ---
    // for v in videos {

    //     println!("{}", v.url);
    //     println!("{}", v.title);
    //     println!("{}", v.duration.as_secs());

    //     if v.channel.is_some() {
    //         println!("{}", v.channel.unwrap());
    //     }

    //     if v.published.is_some() {
    //         println!("{}", v.published.unwrap());
    //     }

    //     println!("-----------------");
    // }
}
